# Open Science Retreat 2023

Welcome to the repository for the Open Science Retreat 2023 :tada:

## Form the program

We will form the program of this event together :handshake: 
Please post ideas for projects, discussion topics, and sessions in [this board](https://chat.science-retreat.org/boards/workspace/qtjgpyyqk3bn3kjwgabh6t8h9r/bdutzkeatcigs9bh3m36iqmkrew/vuxdxajh8t78edebh4t3zcexp9e). You need to have a Mattermost account in order to do so. You received the invite with the sign up to the event.

<!--
- :bulb: [Open a new issue](https://gitlab.com/open-science-retreat/open-science-retreat-2023/-/issues/new) for a new idea.
- :cherries: Comment on existing [issues](https://gitlab.com/open-science-retreat/open-science-retreat-2023/-/issues) if you have any thoughts or questions about it.

:information_desk_person_tone2: 
*Please make sure that your posts are understandable to everyone. We come from different fields. Use as little jargon as possible.* 
:information_desk_person_tone2:

We will decide on the final program on the first day of the event.

If you run into any problems with this, please [get in touch](https://open-science-retreat.gitlab.io/contact/).


## Chat with other participants + organising team

We'll set up a Mattermost chat where anyone can ask questions and connect with others interested in the Open Science Retreat. 
You will receive a link once you've been accepted to participate in the retreat.

-->